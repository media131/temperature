import java.util.Scanner;
public class Temperature 
{
	static Scanner keyboard = new Scanner(System.in);
	public static int numberOfMonths = 12; //keyboard.nextInt(); to manually input number of months
	public static int highAndLowTemperature = 2; //keyboard.nextInt(); to manually input number of number of temperatures each month
	public static float [][] tempArray = new float[numberOfMonths][highAndLowTemperature];
	public static float [] highestTempArray = new float[highAndLowTemperature];
	public static float [] lowestTempArray = new float[highAndLowTemperature];
	public static float max;
	public static float min;
	public static float highestHigh=-999999999;
	public static float lowestLow  = 999999999;
	
	public static void main(String []args)
	{
		inputTempForYear();
		
		System.out.println("");
		System.out.println("Average High Temperature: "+calculateAverageHigh(tempArray));
		System.out.println("");
		System.out.println("Average Low Temperature: "+calculateAverageLow(tempArray));
		
		float[] high = findHighestTemp(tempArray);
		float[] low  = findLowestTemp(tempArray);		
		
		int sumHigh =(int) (high[1]+1);
		int sumLow  =(int) ( low[1]+1);
		
		System.out.println("");
		if (sumHigh==1)
		{System.out.println("The Month is January.");
		sumHigh++;
		}else if (sumHigh==2)
		{System.out.println("The Month is February.");
		sumHigh++;
		}else if (sumHigh==3)
		{System.out.println("The Month is March.");
		sumHigh++;
		}else if (sumHigh==4)
		{System.out.println("The Month is April");
		sumHigh++;
		}else if (sumHigh==5)
		{System.out.println("The Month is May");
		sumHigh++;
		}else if (sumHigh==6)
		{System.out.println("The Month is June");
		sumHigh++;
		}else if (sumHigh==7)
		{System.out.println("The Month is July");
		sumHigh++;
		}else if (sumHigh==8)
		{System.out.println("The Month is August");
		sumHigh++;
		}else if (sumHigh==9)
		{System.out.println("The Month is September");
		sumHigh++;
		}else if (sumHigh==10)
		{System.out.println("The Month is October");
		sumHigh++;
		}else if (sumHigh==11)
		{System.out.println("The Month is November");
		sumHigh++;
		}else {System.out.println("The Month is December.");
		sumHigh++;}
		System.out.println("Highest High Temperature: "+high[0]);		
		
		System.out.println("");
		if (sumLow==1)
		{System.out.println("The Month is January.");
		sumLow++;
		}else if (sumLow==2)
		{System.out.println("The Month is February.");
		sumLow++;
		}else if (sumLow==3)
		{System.out.println("The Month is March.");
		sumLow++;
		}else if (sumLow==4)
		{System.out.println("The Month is April.");
		sumLow++;
		}else if (sumLow==5)
		{System.out.println("The Month is May.");
		sumLow++;
		}else if (sumLow==6)
		{System.out.println("The Month is June.");
		sumLow++;
		}else if (sumLow==7)
		{System.out.println("The Month is July.");
		sumLow++;
		}else if (sumLow==8)
		{System.out.println("The Month is August.");
		sumLow++;
		}else if (sumLow==9)
		{System.out.println("The Month is September.");
		sumLow++;
		}else if (sumLow==10)
		{System.out.println("The Month is October.");
		sumLow++;
		}else if (sumLow==11)
		{System.out.println("The Month is November.");
		sumLow++;
		}else {System.out.println("The Month is December.");
		sumLow++;}
		System.out.println("Lowest Low Temperature: "+low[0]);
	}
	
	private static float inputTempForMonth(float input) 
	{
		return input=keyboard.nextFloat();
	}

	private static float[][] inputTempForYear() 
	{
		int monthNumber=1;		
		for (int month = 0; month < numberOfMonths; month++) 
		{
			System.out.print("Enter the high and low temperature for ");
			if (monthNumber==1)
			{System.out.println("January.");
			monthNumber++;
			}else if (monthNumber==2)
			{System.out.println("February.");
			monthNumber++;
			}else if (monthNumber==3)
			{System.out.println("March.");
			monthNumber++;
			}else if (monthNumber==4)
			{System.out.println("April.");
			monthNumber++;
			}else if (monthNumber==5)
			{System.out.println("May.");
			monthNumber++;
			}else if (monthNumber==6)
			{System.out.println("June.");
			monthNumber++;
			}else if (monthNumber==7)
			{System.out.println("July.");
			monthNumber++;
			}else if (monthNumber==8)
			{System.out.println("August.");
			monthNumber++;
			}else if (monthNumber==9)
			{System.out.println("September.");
			monthNumber++;
			}else if (monthNumber==10)
			{System.out.println("October.");
			monthNumber++;
			}else if (monthNumber==11)
			{System.out.println("November.");
			monthNumber++;
			}else {System.out.println("December.");
			monthNumber++;}
			
			for (int tempNumber = 0; tempNumber < highAndLowTemperature; tempNumber++) 
			{
				int displayTempNumber =tempNumber+1; //To display numbers 1-2 instead of 0-1 for clarity
				System.out.println("Enter temperature "+displayTempNumber);
				
				
				tempArray[month][tempNumber]=inputTempForMonth(0.0f);
			}			
		}
		return tempArray;
	}

	private static float calculateAverageHigh(float tempArray[][])
	{
		float totalSumMax=0;
		float [][] averageHigh = new float[numberOfMonths][highAndLowTemperature];
		for(int i=0; i<numberOfMonths; ++i)
		{
			for(int j=0; j<highAndLowTemperature; ++j)
			{
				averageHigh[i][j] = tempArray[i][j];
			}
			
			if (averageHigh[i][0]>averageHigh[i][1])
			{
				max=averageHigh[i][0];
			}
			else
			{
				max=averageHigh[i][1];
			}			
			totalSumMax=totalSumMax+max;
		}
		float averageHighTemp = totalSumMax/numberOfMonths;
		return averageHighTemp;
	}

	private static float calculateAverageLow(float tempArray[][]) 
	{
		float totalSumMin=0;
		float [][] averageLow = new float[numberOfMonths][highAndLowTemperature];
		for(int i=0; i<numberOfMonths; ++i)
		{
			for(int j=0; j<highAndLowTemperature; ++j)
			{
				averageLow[i][j] = tempArray[i][j];
			}
			
			if (averageLow[i][0]>averageLow[i][1])
			{
				min=averageLow[i][1];
			}
			else
			{
				min=averageLow[i][0];
			}		
			totalSumMin=totalSumMin+min;
		}
		float averageLowTemp = totalSumMin/numberOfMonths;
		return averageLowTemp;
	}

	private static float[] findHighestTemp(float tempArray[][])
	{
		float [][] highestTemp = new float[numberOfMonths][highAndLowTemperature];
		for(int i=0; i<numberOfMonths; ++i)
		{
			for(int j=0; j<highAndLowTemperature; ++j)
			{
				highestTemp[i][j] = tempArray[i][j];
				if (highestTemp[i][j]>highestHigh)
				{
					highestHigh= tempArray[i][j];		
					highestTempArray[0]= highestHigh;
					int monthNumber=i;
					highestTempArray[1]=monthNumber;
				}
			}			
		}
		return highestTempArray;
	}

	private static float[] findLowestTemp(float tempArray[][]) 
	{
		float [][] lowestTemp = new float[numberOfMonths][highAndLowTemperature];
		for(int i=0; i<numberOfMonths; ++i)
		{
			for(int j=0; j<highAndLowTemperature; ++j)
			{
				lowestTemp[i][j] = tempArray[i][j];
				if (lowestTemp[i][j]<lowestLow)
				{
					lowestLow= tempArray[i][j];
					lowestTempArray[0]=lowestLow;
					int monthNumber=i;
					lowestTempArray[1]=monthNumber;
				}
			}
		}
		return lowestTempArray;
	}
}



